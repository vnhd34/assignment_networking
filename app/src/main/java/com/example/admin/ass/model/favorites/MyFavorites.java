package com.example.admin.ass.model.favorites;

import android.os.Parcel;
import android.os.Parcelable;

public class MyFavorites implements Parcelable{

    private String mId;
    private String mUrl;
    private String mView;
    private String mLike;



    public MyFavorites(String id, String url, String view, String like) {
        mId = id;
        mUrl = url;
        mView = view;
        mLike = like;
    }


    public MyFavorites(String url, String view, String like) {
        mUrl = url;
        mView = view;
        mLike = like;
    }


    public MyFavorites() {
        //for sql
    }



    protected MyFavorites(Parcel in) {
        mId = in.readString();
        mUrl = in.readString();
        mView = in.readString();
        mLike = in.readString();
    }



    public static final Creator<MyFavorites> CREATOR = new Creator<MyFavorites>() {
        @Override
        public MyFavorites createFromParcel(Parcel in) {
            return new MyFavorites(in);
        }



        @Override
        public MyFavorites[] newArray(int size) {
            return new MyFavorites[size];
        }
    };



    public String getId() {
        return mId;
    }



    public void setId(String id) {
        mId = id;
    }



    public String getUrl() {
        return mUrl;
    }



    public void setUrl(String url) {
        mUrl = url;
    }



    public String getView() {
        return mView;
    }



    public void setView(String view) {
        mView = view;
    }



    public String getLike() {
        return mLike;
    }



    public void setLike(String like) {
        mLike = like;
    }



    @Override
    public int describeContents() {
        return 0;
    }



    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mId);
        parcel.writeString(mUrl);
        parcel.writeString(mView);
        parcel.writeString(mLike);
    }
}
