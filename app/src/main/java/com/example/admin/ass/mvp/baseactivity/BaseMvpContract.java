package com.example.admin.ass.mvp.baseactivity;

import android.support.v4.app.Fragment;

public interface BaseMvpContract {

    interface View<P> {
        void initView();

        void afterInit();

        void showToast(String message);

        P getPresenter();

        void switchActivity(Class<?> target);

        void switchFragment(int parentId, Fragment target);
    }

    interface Presenter {

    }
}
