
package com.example.admin.ass.model.gif;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HDWALLPAPER implements Parcelable{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("gif_image")
    @Expose
    private String gifImage;
    @SerializedName("total_views")
    @Expose
    private String totalViews;



    protected HDWALLPAPER(Parcel in) {
        id = in.readString();
        gifImage = in.readString();
        totalViews = in.readString();
    }



    public static final Creator<HDWALLPAPER> CREATOR = new Creator<HDWALLPAPER>() {
        @Override
        public HDWALLPAPER createFromParcel(Parcel in) {
            return new HDWALLPAPER(in);
        }



        @Override
        public HDWALLPAPER[] newArray(int size) {
            return new HDWALLPAPER[size];
        }
    };



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGifImage() {
        return gifImage;
    }

    public void setGifImage(String gifImage) {
        this.gifImage = gifImage;
    }

    public String getTotalViews() {
        return totalViews;
    }

    public void setTotalViews(String totalViews) {
        this.totalViews = totalViews;
    }



    @Override
    public int describeContents() {
        return 0;
    }



    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(gifImage);
        parcel.writeString(totalViews);
    }
}
