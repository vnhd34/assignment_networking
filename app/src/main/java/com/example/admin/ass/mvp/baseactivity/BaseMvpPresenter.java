package com.example.admin.ass.mvp.baseactivity;

public abstract class BaseMvpPresenter<V> implements BaseMvpContract.Presenter {
    protected V mView;

    public BaseMvpPresenter(V view) {
        mView = view;
    }
}
