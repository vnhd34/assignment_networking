package com.example.admin.ass.ui.fragment.latest;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.admin.ass.R;
import com.example.admin.ass.model.latest.HdWallPaper;
import com.example.admin.ass.mvp.basefragment.BaseMvpFragment;
import com.example.admin.ass.ui.activity.imagedetail.ImagDetailActivity;
import com.example.admin.ass.ui.adapter.LatestAdapter;

import java.util.List;

public class LatestFragment extends BaseMvpFragment<LatestContract.Presenter> implements  LatestContract.View {

    private RecyclerView mRecyclerView;
    private LatestAdapter mAdapter;
    private ProgressDialog mDialog;

    public LatestFragment() {
        super(R.layout.fragment_latest);
    }



    @Override
    public void initView(View view) {
        mRecyclerView = view.findViewById(R.id.rv);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
    }



    @Override
    public void afterInit() {
        mPresenter.setUpData();
    }



    @Override
    public LatestContract.Presenter getPreseter() {
        return new LatestPresenter(this);
    }



    @Override
    public void setUpRecyclerView(List<HdWallPaper> hdwallpaper) {
        mAdapter = new LatestAdapter(hdwallpaper, getActivity());
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.onClickListener(mPresenter);
    }



    @Override
    public void showDialogLoading() {
        mDialog = new ProgressDialog(getActivity());
        mDialog.setTitle("Loading");
        mDialog.setMessage("Hold on");
        mDialog.show();
    }



    @Override
    public void hideDialogLoading() {
        mDialog.dismiss();
    }



    @Override
    public void goToDetailActivity(HdWallPaper hdWallPaper) {
        Intent intent = new Intent(getActivity(), ImagDetailActivity.class);
        intent.putExtra(ImagDetailActivity.KEY_IMAGE,hdWallPaper );
        startActivity(intent);
    }
}
