package com.example.admin.ass.event;

public interface ClickListener {
    void onClickItem(int position);
}
