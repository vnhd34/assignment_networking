
package com.example.admin.ass.model.latest;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Latest {

    @SerializedName("HD_WALLPAPER")
    @Expose
    private List<HdWallPaper> mHdWallPapers = null;



    public List<HdWallPaper> getHDWALLPAPER() {
        return mHdWallPapers;
    }



    public void setHDWALLPAPER(List<HdWallPaper> hdwallpaper) {
        this.mHdWallPapers = hdwallpaper;
    }

}
