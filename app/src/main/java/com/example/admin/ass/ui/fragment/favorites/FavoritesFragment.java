package com.example.admin.ass.ui.fragment.favorites;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.admin.ass.R;
import com.example.admin.ass.model.favorites.MyFavorites;
import com.example.admin.ass.model.latest.HdWallPaper;
import com.example.admin.ass.mvp.basefragment.BaseMvpFragment;
import com.example.admin.ass.ui.activity.imagedetail.ImagDetailActivity;
import com.example.admin.ass.ui.adapter.FavoritesAdapter;
import com.example.admin.ass.ui.adapter.LatestAdapter;
import com.example.admin.ass.ui.fragment.detailcategory.DetailCategoryFragment;

import java.util.List;

public class FavoritesFragment extends BaseMvpFragment<FavoritesContract.Presenter> implements  FavoritesContract.View {

    private RecyclerView mRecyclerView;
    private FavoritesAdapter mAdapter;
    private ProgressDialog mDialog;

    public FavoritesFragment() {
        super(R.layout.fragment_favorrites);
    }



    @Override
    public void initView(View view) {
        mRecyclerView = view.findViewById(R.id.rv);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
    }



    @Override
    public void afterInit() {
        mPresenter.setUpData();
    }



    @Override
    public FavoritesContract.Presenter getPreseter() {
        return new FavoritesPresenter(this);
    }



    @Override
    public void setUpRecyclerView(List<MyFavorites> hdwallpaper) {
        mAdapter = new FavoritesAdapter(hdwallpaper, getActivity());
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.onClickListener(mPresenter);
    }



    @Override
    public void showDialogLoading() {
        mDialog = new ProgressDialog(getActivity());
        mDialog.setTitle("Loading");
        mDialog.setMessage("Hold on");
        mDialog.show();
    }



    @Override
    public void hideDialogLoading() {
        mDialog.dismiss();
    }



    @Override
    public void goToDetailActivity(MyFavorites hdWallPaper) {
        Intent intent = new Intent(getActivity(), ImagDetailActivity.class);
        intent.putExtra(ImagDetailActivity.KEY_IMAGE, hdWallPaper );
        startActivity(intent);
    }



    @Override
    public void onResume() {
        super.onResume();
        mPresenter.setUpData();
    }



    @Override
    public Context getContextActivity() {
        return getContext();
    }
}
