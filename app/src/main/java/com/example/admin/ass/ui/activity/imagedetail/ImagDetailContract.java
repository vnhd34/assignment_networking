package com.example.admin.ass.ui.activity.imagedetail;

import android.app.Activity;
import android.content.Context;

import com.example.admin.ass.model.latest.HdWallPaper;
import com.example.admin.ass.mvp.baseactivity.BaseMvpContract;

public interface ImagDetailContract {

    interface View extends BaseMvpContract.View<Presenter>{
        void setUpView(String url);
        Context getContextActivity();
    }
    
    interface Presenter{
        void setUpDataLatest(Object hd);

        void onSaveFavorites();

        void onCreateView(Context contextActivity);
    }
}
