package com.example.admin.ass.ui.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.admin.ass.R;
import com.example.admin.ass.event.ClickListener;
import com.example.admin.ass.model.category.HDWALLPAPER;
import com.example.admin.ass.util.ImageHelper;
import com.facebook.drawee.view.SimpleDraweeView;

import java.io.UnsupportedEncodingException;
import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {
    private List<HDWALLPAPER> mWallPapers;
    private Activity mContext;
    private ClickListener mListener;



    public CategoryAdapter(List<HDWALLPAPER> wallPapers, Activity context) {
        mWallPapers = wallPapers;
        mContext = context;
    }


    public void onClickItem(ClickListener listener){
        mListener = listener;
    }


    @NonNull
    @Override
    public CategoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.layout_item_category, parent, false));
    }



    @Override
    public void onBindViewHolder(@NonNull CategoryAdapter.ViewHolder holder, int position) {
        try {
            holder.bindWitthData(mWallPapers.get(position));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }



    @Override
    public int getItemCount() {
        return mWallPapers.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private SimpleDraweeView mImage;
        private TextView mName;
        private TextView mTotal;
        int[] mScreen;

        public ViewHolder(View itemView) {
            super(itemView);
            mImage = itemView.findViewById(R.id.imageSimpleDrawee);
            mName = itemView.findViewById(R.id.nameCategoryTextview);
            mTotal = itemView.findViewById(R.id.totalCategoryTextview);
            mScreen = ImageHelper.getDeviceMetrics(mContext);
            itemView.setOnClickListener(this);
        }

        @SuppressLint("SetTextI18n")
        public void bindWitthData(HDWALLPAPER hdWallPaper) throws UnsupportedEncodingException {
            ImageHelper.loadImage(hdWallPaper.getCategoryImage().replaceAll(" ", "%20"), mImage, mScreen[0], 140);
            mImage.getHierarchy().setProgressBarImage(ImageHelper.progressBarLoading(mContext));
            mName.setText(hdWallPaper.getCategoryName());
            mTotal.setText("("+hdWallPaper.getTotalWallpaper()+")");
        }



        @Override
        public void onClick(View view) {
            mListener.onClickItem(getAdapterPosition());
        }
    }


}
