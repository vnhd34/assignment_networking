package com.example.admin.ass.ui.fragment.about;

import com.example.admin.ass.model.about.HDWALLPAPER;
import com.example.admin.ass.mvp.basefragment.BaseMvpContract;

public interface AboutContract {

    interface View extends BaseMvpContract.View<Presenter>{
        void setUpViewWithData(HDWALLPAPER data);
    }
    
    interface Presenter{

        void setUpData();
    }
}
