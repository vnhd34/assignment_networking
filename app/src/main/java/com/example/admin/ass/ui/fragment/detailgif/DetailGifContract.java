package com.example.admin.ass.ui.fragment.detailgif;

import com.example.admin.ass.event.ClickListener;
import com.example.admin.ass.model.gif.HDWALLPAPER;
import com.example.admin.ass.model.latest.HdWallPaper;
import com.example.admin.ass.mvp.basefragment.BaseMvpContract;

import java.util.List;

public interface DetailGifContract {

    interface View extends BaseMvpContract.View<Presenter>{

        void setUpRecyclerView(List<HDWALLPAPER> hdwallpaper);

        void showDialogLoading();

        void hideDialogLoading();

        void goToDetailActivity(HDWALLPAPER hdWallPaper);
    }
    
    interface Presenter extends ClickListener {

        void setUpData(String id);
    }
}
