package com.example.admin.ass.ui.activity.imagedetail;

import android.content.Context;

import com.example.admin.ass.db.SQLiteHelperMe;
import com.example.admin.ass.model.favorites.MyFavorites;
import com.example.admin.ass.model.gif.HDWALLPAPER;
import com.example.admin.ass.model.latest.HdWallPaper;
import com.example.admin.ass.mvp.baseactivity.BaseMvpPresenter;

import java.util.List;

public class ImagDetailPresenter extends BaseMvpPresenter<ImagDetailContract.View> implements ImagDetailContract.Presenter {

    private List<MyFavorites> mList;
    private MyFavorites mMyFavorites;
    private SQLiteHelperMe mDB;



    public ImagDetailPresenter(ImagDetailContract.View view) {
        super(view);

    }



    @Override
    public void onCreateView(Context contextActivity) {
        mDB = SQLiteHelperMe.getInstance(mView.getContextActivity());
    }



    @Override
    public void setUpDataLatest(Object hd) {
        if (hd instanceof HdWallPaper) {
            HdWallPaper hdWallPaper = (HdWallPaper) hd;
            mView.setUpView(hdWallPaper.getWallpaperImage());
            mMyFavorites = new MyFavorites(hdWallPaper.getWallpaperImage(), hdWallPaper.getTotalViews(), hdWallPaper.getCatId());
        }
        if (hd instanceof HDWALLPAPER) {
            HDWALLPAPER hdWallPaper = (HDWALLPAPER) hd;
            mView.setUpView(hdWallPaper.getGifImage());
            mMyFavorites = new MyFavorites(hdWallPaper.getGifImage(), hdWallPaper.getTotalViews(), hdWallPaper.getId());
        }
        if (hd instanceof MyFavorites) {
            MyFavorites hdWallPaper = (MyFavorites) hd;
            mView.setUpView(hdWallPaper.getUrl());
            mMyFavorites = new MyFavorites(hdWallPaper.getUrl(), hdWallPaper.getView(), hdWallPaper.getId());
        }

    }



    @Override
    public void onSaveFavorites() {
        if (mMyFavorites == null) {
            return;
        }
        mList = mDB.getAllFavorites();
        for (MyFavorites x : mList){
            if(x.getUrl().equalsIgnoreCase(mMyFavorites.getUrl())){
                mDB.delete(x.getUrl());
                mView.showToast("Bạn xóa Favorites");
                return;
            }
        }
        mDB.addPost(mMyFavorites, mView.getContextActivity());
    }

}
