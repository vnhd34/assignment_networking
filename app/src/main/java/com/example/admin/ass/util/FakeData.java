package com.example.admin.ass.util;

import com.example.admin.ass.model.latest.HdWallPaper;

import java.util.ArrayList;
import java.util.List;

public class FakeData {

    public static List<HdWallPaper> getImage() {
        List<HdWallPaper> latests = new ArrayList<>();
        latests.add(new HdWallPaper(
                "01",
                "01",
                "https://media.ngoisao.vn/resize_580/news/2018/01/31/ngoc-trinh-xinh-dep-quyen-ru-1resize-ngoisao.vn-w580-h870.stamp2.JPG",
                "https://media.ngoisao.vn/resize_580/news/2018/01/31/ngoc-trinh-xinh-dep-quyen-ru-1resize-ngoisao.vn-w580-h870.stamp2.JPG",
                "10",
                "12",
                "Never Enough",
                "https://media.ngoisao.vn/resize_580/news/2018/01/31/ngoc-trinh-xinh-dep-quyen-ru-1resize-ngoisao.vn-w580-h870.stamp2.JPG",
                "https://media.ngoisao.vn/resize_580/news/2018/01/31/ngoc-trinh-xinh-dep-quyen-ru-1resize-ngoisao.vn-w580-h870.stamp2.JPG"
        ));

        latests.add(new HdWallPaper(
                "01",
                "01",
                "http://ttol.vietnamnetjsc.vn//2017/06/05/11/32/nguoi-mau-noi-y-ngoc-trinh-lau-lam-roi-toi-chua-co-bo_3.jpg",
                "http://ttol.vietnamnetjsc.vn//2017/06/05/11/32/nguoi-mau-noi-y-ngoc-trinh-lau-lam-roi-toi-chua-co-bo_3.jpg",
                "2",
                "456",
                "Never Enough",
                "http://ttol.vietnamnetjsc.vn//2017/06/05/11/32/nguoi-mau-noi-y-ngoc-trinh-lau-lam-roi-toi-chua-co-bo_3.jpg",
                "http://ttol.vietnamnetjsc.vn//2017/06/05/11/32/nguoi-mau-noi-y-ngoc-trinh-lau-lam-roi-toi-chua-co-bo_3.jpg"
        ));

        latests.add(new HdWallPaper(
                "01",
                "-1",
                "http://i.chieu-cao.net/wp-content/uploads/2017/02/chieu-cao-va-tieu-su-cua-ngoc-trinh-5.jpg",
                "http://i.chieu-cao.net/wp-content/uploads/2017/02/chieu-cao-va-tieu-su-cua-ngoc-trinh-5.jpg",
                "2",
                "456",
                "Never Enough",
                "http://i.chieu-cao.net/wp-content/uploads/2017/02/chieu-cao-va-tieu-su-cua-ngoc-trinh-5.jpg",
                "http://i.chieu-cao.net/wp-content/uploads/2017/02/chieu-cao-va-tieu-su-cua-ngoc-trinh-5.jpg"
        ));

        latests.add(new HdWallPaper(
                "01",
                "-1",
                "http://i.chieu-cao.net/wp-content/uploads/2017/02/chieu-cao-va-tieu-su-cua-ngoc-trinh-5.jpg",
                "http://i.chieu-cao.net/wp-content/uploads/2017/02/chieu-cao-va-tieu-su-cua-ngoc-trinh-5.jpg",
                "2",
                "456",
                "Never Enough",
                "http://i.chieu-cao.net/wp-content/uploads/2017/02/chieu-cao-va-tieu-su-cua-ngoc-trinh-5.jpg",
                "http://i.chieu-cao.net/wp-content/uploads/2017/02/chieu-cao-va-tieu-su-cua-ngoc-trinh-5.jpg"
        ));

        latests.add(new HdWallPaper(
                "01",
                "-1",
                "http://i.chieu-cao.net/wp-content/uploads/2017/02/chieu-cao-va-tieu-su-cua-ngoc-trinh-5.jpg",
                "http://i.chieu-cao.net/wp-content/uploads/2017/02/chieu-cao-va-tieu-su-cua-ngoc-trinh-5.jpg",
                "2",
                "456",
                "Never Enough",
                "http://i.chieu-cao.net/wp-content/uploads/2017/02/chieu-cao-va-tieu-su-cua-ngoc-trinh-5.jpg",
                "http://i.chieu-cao.net/wp-content/uploads/2017/02/chieu-cao-va-tieu-su-cua-ngoc-trinh-5.jpg"
        ));

        latests.add(new HdWallPaper(
                "01",
                "01",
                "http://ttol.vietnamnetjsc.vn//2017/06/05/11/32/nguoi-mau-noi-y-ngoc-trinh-lau-lam-roi-toi-chua-co-bo_3.jpg",
                "http://ttol.vietnamnetjsc.vn//2017/06/05/11/32/nguoi-mau-noi-y-ngoc-trinh-lau-lam-roi-toi-chua-co-bo_3.jpg",
                "2",
                "456",
                "Never Enough",
                "http://ttol.vietnamnetjsc.vn//2017/06/05/11/32/nguoi-mau-noi-y-ngoc-trinh-lau-lam-roi-toi-chua-co-bo_3.jpg",
                "http://ttol.vietnamnetjsc.vn//2017/06/05/11/32/nguoi-mau-noi-y-ngoc-trinh-lau-lam-roi-toi-chua-co-bo_3.jpg"
        ));
        return latests;
    }
}
