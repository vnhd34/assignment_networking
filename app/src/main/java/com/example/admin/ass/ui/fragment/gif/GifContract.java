package com.example.admin.ass.ui.fragment.gif;

import com.example.admin.ass.event.ClickListener;
import com.example.admin.ass.model.gif.HDWALLPAPER;
import com.example.admin.ass.model.latest.HdWallPaper;
import com.example.admin.ass.mvp.basefragment.BaseMvpContract;

import java.util.List;

public interface GifContract {

    interface View extends BaseMvpContract.View<Presenter>{

        void setUpRecyclerView(List<HDWALLPAPER> hdwallpaper);

        void showDialogLoading();

        void hideDialogLoading();

        void goToFragmemntDetailGif(String id);
    }
    
    interface Presenter extends ClickListener {

        void setUpData();
    }
}
