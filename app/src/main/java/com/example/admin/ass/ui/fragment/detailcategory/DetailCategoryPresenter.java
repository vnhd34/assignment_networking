package com.example.admin.ass.ui.fragment.detailcategory;

import com.example.admin.ass.event.DataListener;
import com.example.admin.ass.model.detailcategory.DetailCategory;
import com.example.admin.ass.model.latest.HdWallPaper;
import com.example.admin.ass.mvp.basefragment.BaseMvpPresenter;
import com.example.admin.ass.rest.ApiRequest;

import java.util.List;


public class DetailCategoryPresenter extends BaseMvpPresenter<DetailCategoryContract.View> implements DetailCategoryContract.Presenter {

    private List<HdWallPaper> mHdWallPapers;
    public DetailCategoryPresenter(DetailCategoryContract.View view) {
        super(view);
    }



    @Override
    public void setUpData(String id) {
        new ApiRequest.RequestDataDetail(listener, new DetailCategoryFragment(), id).execute();
//        mView.showDialogLoading();
    }



    private DataListener listener = new DataListener() {
        @Override
        public void onResquest(Object target) {
//            if (target instanceof DetailCategory) {
                mHdWallPapers = ((DetailCategory) target).getHDWALLPAPER();
//                mView.hideDialogLoading();
                mView.setUpRecyclerView(mHdWallPapers);
//            }
        }



        @Override
        public void onError(String error) {
            mView.hideDialogLoading();
            mView.showToast(error);
        }
    };



    @Override
    public void onClickItem(int position) {
        mView.goToDetailActivity(mHdWallPapers.get(position));
    }
}
