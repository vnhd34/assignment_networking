package com.example.admin.ass.mvp.baseactivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

public abstract class BaseMvpActivity<P> extends AppCompatActivity implements BaseMvpContract.View<P> {
    private int mViewId;
    protected P mPresenter;



    public BaseMvpActivity(int viewId) {
        mViewId = viewId;
        mPresenter = getPresenter();
    }



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(mViewId);
        initView();
        afterInit();
    }



    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }



    @Override
    public void switchActivity(Class<?> target) {
        startActivity(new Intent(this, target));
    }



    @Override
    public void switchFragment(int parentId, Fragment target) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(parentId, target)
                .addToBackStack(target.getClass().getName())
                .commit();
    }
}
