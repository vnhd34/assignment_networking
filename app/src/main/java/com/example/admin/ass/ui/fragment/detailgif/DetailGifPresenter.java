package com.example.admin.ass.ui.fragment.detailgif;

import com.example.admin.ass.event.DataListener;
import com.example.admin.ass.model.detailcategory.DetailCategory;
import com.example.admin.ass.model.gif.Gif;
import com.example.admin.ass.model.gif.HDWALLPAPER;
import com.example.admin.ass.model.latest.HdWallPaper;
import com.example.admin.ass.mvp.basefragment.BaseMvpPresenter;
import com.example.admin.ass.rest.ApiRequest;

import java.util.List;


public class DetailGifPresenter extends BaseMvpPresenter<DetailGifContract.View> implements DetailGifContract.Presenter {

    private List<HDWALLPAPER> mHdWallPapers;
    public DetailGifPresenter(DetailGifContract.View view) {
        super(view);
    }



    @Override
    public void setUpData(String id) {
        new ApiRequest.RequestDataDetail(listener, new DetailGifFragment(), id).execute();
//        mView.showDialogLoading();
    }



    private DataListener listener = new DataListener() {
        @Override
        public void onResquest(Object target) {
            if (target instanceof Gif) {
                mHdWallPapers = ((Gif) target).getHDWALLPAPER();
//                mView.hideDialogLoading();
                mView.setUpRecyclerView(mHdWallPapers);
            }
        }



        @Override
        public void onError(String error) {
//            mView.hideDialogLoading();
            mView.showToast(error);
        }
    };



    @Override
    public void onClickItem(int position) {
        mView.goToDetailActivity(mHdWallPapers.get(position));
    }
}
