package com.example.admin.ass.ui.fragment.category;

import android.app.ProgressDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.admin.ass.R;
import com.example.admin.ass.model.category.HDWALLPAPER;
import com.example.admin.ass.model.latest.HdWallPaper;
import com.example.admin.ass.mvp.basefragment.BaseMvpFragment;
import com.example.admin.ass.ui.adapter.CategoryAdapter;
import com.example.admin.ass.ui.fragment.detailcategory.DetailCategoryFragment;

import java.util.List;

public class CategoryFragment extends BaseMvpFragment<CategoryContract.Presenter> implements  CategoryContract.View {

    private RecyclerView mRecyclerView;
    private CategoryAdapter mAdapter;
    private ProgressDialog mDialog;

    public CategoryFragment() {
        super(R.layout.fragment_category);
    }



    @Override
    public void initView(View view) {
        mRecyclerView = view.findViewById(R.id.rv);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
    }



    @Override
    public void afterInit() {
        mPresenter.setUpData();
    }



    @Override
    public CategoryContract.Presenter getPreseter() {
        return new CategoryPresenter(this);
    }



    @Override
    public void setUpRecyclerView(List<HDWALLPAPER> hdwallpaper) {
        mAdapter = new CategoryAdapter(hdwallpaper, getActivity());
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.onClickItem(mPresenter);
    }



    @Override
    public void showDialogLoading() {
        mDialog = new ProgressDialog(getActivity());
        mDialog.setTitle("Loading");
        mDialog.setMessage("Hold on");
        mDialog.show();
    }



    @Override
    public void hideDialogLoading() {
        mDialog.dismiss();
    }



    @Override
    public void goToFragmemntDetailCategory(String id){
        DetailCategoryFragment fragment = new DetailCategoryFragment();
        fragment.getImage(id);
        switchFragment(R.id.myLayout, fragment);
    }
}
