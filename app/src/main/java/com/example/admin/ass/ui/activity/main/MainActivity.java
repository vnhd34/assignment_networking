package com.example.admin.ass.ui.activity.main;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.example.admin.ass.R;
import com.example.admin.ass.mvp.baseactivity.BaseMvpActivity;
import com.example.admin.ass.ui.fragment.about.AboutFragment;
import com.example.admin.ass.ui.fragment.category.CategoryFragment;
import com.example.admin.ass.ui.fragment.favorites.FavoritesFragment;
import com.example.admin.ass.ui.fragment.gif.GifFragment;
import com.example.admin.ass.ui.activity.imagedetail.ImagDetailActivity;
import com.example.admin.ass.ui.fragment.latest.LatestFragment;

import java.util.Objects;

public class MainActivity extends BaseMvpActivity<MainContract.Presenter> implements MainContract.View, View.OnClickListener {
    public static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    public static Toolbar mToolbar;
    private Fragment[] mFragments;



    public MainActivity() {
        super(R.layout.activity_main);
    }



    @Override
    public void initView() {
        mDrawerLayout = findViewById(R.id.drawer_layout);
        mNavigationView = findViewById(R.id.nav_view);
        mToolbar = findViewById(R.id.my_toolbar);
    }



    @Override
    public void afterInit() {
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_dehaze);
        mNavigationView.setNavigationItemSelectedListener(mListener);

        mFragments = new Fragment[]{
                new AboutFragment(),
                new CategoryFragment(),
                new FavoritesFragment(),
                new LatestFragment(),
                new GifFragment()
        };
        switchFragment(R.id.myLayout, mFragments[3]);
        checkPermission();
    }



    @Override
    public MainContract.Presenter getPresenter() {
        return new MainPresenter(this);
    }



    private NavigationView.OnNavigationItemSelectedListener mListener = new NavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            item.setChecked(true);
            mDrawerLayout.closeDrawers();
            mToolbar.setTitle(item.getTitle());
            switch (item.getItemId()) {
                case R.id.lates:
                    switchFragment(R.id.myLayout, mFragments[3]);
                    return true;
                case R.id.category:
                    switchFragment(R.id.myLayout, mFragments[1]);
                    return true;
                case R.id.gifs:
                    switchFragment(R.id.myLayout, mFragments[4]);
                    return true;
                case R.id.favorites:
                    switchFragment(R.id.myLayout, mFragments[2]);
                    return true;
                case R.id.about_us:
                    switchFragment(R.id.myLayout, mFragments[0]);
                    return true;
                case R.id.settings:
                    return true;
            }
            return true;
        }
    };



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void checkPermission(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
            }
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                }
            }
        }
    }



    @Override
    public void onClick(View view) {
    }
}
