package com.example.admin.ass.ui.activity.imagedetail;

import android.app.Activity;
import android.app.WallpaperManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Animatable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.view.View;
import android.widget.ImageView;

import com.example.admin.ass.R;
import com.example.admin.ass.db.SQLiteHelperMe;
import com.example.admin.ass.mvp.baseactivity.BaseMvpActivity;
import com.example.admin.ass.ui.activity.main.MainActivity;
import com.example.admin.ass.util.ImageHelper;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.backends.pipeline.PipelineDraweeControllerBuilder;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.imagepipeline.image.ImageInfo;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import me.relex.photodraweeview.PhotoDraweeView;

public class ImagDetailActivity extends BaseMvpActivity<ImagDetailContract.Presenter> implements ImagDetailContract.View, View.OnClickListener {

    public static final String KEY_IMAGE = "com.example.admin.ass.ui.activity.imagedetail_KEY_IMAGE";
    private static final String IMAGE_DIRECTORY = "/demonuts";
    private PhotoDraweeView mSimpleDraweeView;
    private ImageView mImageView;
    private FloatingActionMenu mActionMenu;
    private FloatingActionButton mLike;
    private FloatingActionButton mShare;
    private FloatingActionButton mSave;
    private FloatingActionButton mSetWall;
    private Bitmap mBitmap;
    private Uri mUri;
    private WallpaperManager mWallpaperManager;


    public ImagDetailActivity() {
        super(R.layout.activity_image_detail);
    }



    @Override
    public void initView() {
        mSimpleDraweeView = findViewById(R.id.image);
        mImageView = findViewById(R.id.backImageView);
        mImageView.setOnClickListener(this);
        mActionMenu = findViewById(R.id.material_design_android_floating_action_menu);
        mLike = findViewById(R.id.like);
        mShare = findViewById(R.id.share);
        mSave = findViewById(R.id.saveImage);
        mSetWall = findViewById(R.id.setWallpaper);

        mImageView.setOnClickListener(this);
        mLike.setOnClickListener(this);
        mShare.setOnClickListener(this);
        mSave.setOnClickListener(this);
        mSetWall.setOnClickListener(this);
        mPresenter.onCreateView(getContextActivity());
    }



    @Override
    public void afterInit() {
        if (getIntent().getParcelableExtra(KEY_IMAGE) != null) {
            mPresenter.setUpDataLatest((getIntent().getParcelableExtra(KEY_IMAGE)));
        }
    }



    @Override
    public ImagDetailContract.Presenter getPresenter() {
        return new ImagDetailPresenter(this);
    }



    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backImageView:
                onBackPressed();
                break;
            case R.id.like:
                mPresenter.onSaveFavorites();
                mActionMenu.close(true);
                break;
            case R.id.share:
                showToast("Coming soon");
                mActionMenu.close(true);
                break;
            case R.id.saveImage:
                mSimpleDraweeView.buildDrawingCache();
                mBitmap = mSimpleDraweeView.getDrawingCache();
                String path = saveImage(mBitmap);
                if (path != null) {
                    showToast("Đã lưu trong thư viện");
                } else {
                    showToast("Có lỗi xảy ra. Vui lòng thử lại sau");
                }
                mActionMenu.close(true);
                break;
            case R.id.setWallpaper:
                mSimpleDraweeView.buildDrawingCache();
                mBitmap = mSimpleDraweeView.getDrawingCache();
                mWallpaperManager = WallpaperManager.getInstance(this);
                try {
                    mWallpaperManager.setBitmap(mBitmap);
                    showToast("Đã cài màn hình nền");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                mActionMenu.close(true);
                break;
        }
    }





    @Override
    public void setUpView(String url) {
        mUri = Uri.parse(url.replaceAll(" ", "%20"));
        PipelineDraweeControllerBuilder controller = Fresco.newDraweeControllerBuilder();
        controller.setUri(mUri);
        controller.setAutoPlayAnimations(true);
        controller.setOldController(mSimpleDraweeView.getController());
        mSimpleDraweeView.getHierarchy().setProgressBarImage(ImageHelper.progressBarLoading(this));
        controller.setControllerListener(new BaseControllerListener<ImageInfo>() {
            @Override
            public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {
                super.onFinalImageSet(id, imageInfo, animatable);
                if (imageInfo == null || mSimpleDraweeView == null) {
                    return;
                }
                mSimpleDraweeView.update(imageInfo.getWidth(), imageInfo.getHeight());
            }
        });
        mSimpleDraweeView.setController(controller.build());
    }



    @Override
    public Context getContextActivity() {
        return getApplicationContext();
    }



    private String saveImage(Bitmap bitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }
        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return null;
    }
}
