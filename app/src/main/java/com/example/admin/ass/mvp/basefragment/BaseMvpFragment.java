package com.example.admin.ass.mvp.basefragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.admin.ass.mvp.baseactivity.BaseMvpActivity;

import java.util.Objects;

public abstract class BaseMvpFragment<P> extends Fragment implements BaseMvpContract.View<P> {
    protected P mPresenter;
    private int mViewId;



    public BaseMvpFragment(int viewId) {
        mViewId = viewId;
        mPresenter = getPreseter();
    }



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(mViewId, container, false);
    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        afterInit();
    }



    @Override
    public void showToast(String messager) {
        Toast.makeText(getContext(), mViewId, Toast.LENGTH_LONG).show();
    }



    @Override
    public void switchFragment(int parentId, Fragment fragment) {
        Objects.requireNonNull((BaseMvpActivity) getActivity()).switchFragment(parentId, fragment);
    }



    @Override
    public void switchActivity(Class<?> target) {
        startActivity(new Intent(getContext(), target));
    }
}
