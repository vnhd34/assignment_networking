package com.example.admin.ass.ui.fragment.latest;

import com.example.admin.ass.event.DataListener;
import com.example.admin.ass.model.latest.HdWallPaper;
import com.example.admin.ass.model.latest.Latest;
import com.example.admin.ass.mvp.basefragment.BaseMvpPresenter;
import com.example.admin.ass.rest.ApiRequest;

import java.util.List;


public class LatestPresenter extends BaseMvpPresenter<LatestContract.View> implements LatestContract.Presenter {

    private List<HdWallPaper> mHdWallPapers;
    public LatestPresenter(LatestContract.View view) {
        super(view);
    }



    @Override
    public void setUpData() {
        new ApiRequest.RequestData(listener, new LatestFragment()).execute();
        mView.showDialogLoading();
    }



    private DataListener listener = new DataListener() {
        @Override
        public void onResquest(Object target) {
            if (target instanceof Latest) {
                mHdWallPapers = ((Latest) target).getHDWALLPAPER();
                mView.hideDialogLoading();
                mView.setUpRecyclerView(mHdWallPapers);
            }
        }



        @Override
        public void onError(String error) {
            mView.hideDialogLoading();
            mView.showToast(error);
        }
    };



    @Override
    public void onClickItem(int position) {
        mView.goToDetailActivity(mHdWallPapers.get(position));
    }
}
