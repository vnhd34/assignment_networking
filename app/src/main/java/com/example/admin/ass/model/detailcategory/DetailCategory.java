
package com.example.admin.ass.model.detailcategory;

import java.util.List;

import com.example.admin.ass.model.latest.HdWallPaper;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetailCategory {

    @SerializedName("HD_WALLPAPER")
    @Expose
    private List<HdWallPaper> hDWALLPAPER = null;

    public List<HdWallPaper> getHDWALLPAPER() {
        return hDWALLPAPER;
    }

    public void setHDWALLPAPER(List<HdWallPaper> hDWALLPAPER) {
        this.hDWALLPAPER = hDWALLPAPER;
    }

}
