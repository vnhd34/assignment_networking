package com.example.admin.ass.rest;

import android.os.AsyncTask;
import android.support.v4.app.Fragment;

import com.example.admin.ass.event.DataListener;
import com.example.admin.ass.model.about.About;
import com.example.admin.ass.model.category.Category;
import com.example.admin.ass.model.detailcategory.DetailCategory;
import com.example.admin.ass.model.gif.Gif;
import com.example.admin.ass.model.latest.Latest;
import com.example.admin.ass.ui.fragment.about.AboutFragment;
import com.example.admin.ass.ui.fragment.category.CategoryFragment;
import com.example.admin.ass.ui.fragment.detailcategory.DetailCategoryFragment;
import com.example.admin.ass.ui.fragment.detailgif.DetailGifFragment;
import com.example.admin.ass.ui.fragment.gif.GifFragment;
import com.example.admin.ass.ui.fragment.latest.LatestFragment;


import retrofit2.Response;

public class ApiRequest {

    public static class RequestData extends AsyncTask<Void , Void, Object>{
        private DataListener mListener;
        private Fragment mKEY;

        public RequestData(DataListener listener, Fragment key) {
            mListener = listener;
            mKEY = key;
        }



        @Override
        protected Object doInBackground(Void... voids) {
            try{
                if (mKEY instanceof LatestFragment){
                    Response<Latest> response = ApiConfig.getClient().getLatest().execute();
                    if (response.body() != null){
                        return response.body();
                    }
                }
                if (mKEY instanceof CategoryFragment){
                    Response<Category> response = ApiConfig.getClient().getCategory().execute();
                    if (response.body() != null){
                        return response.body();
                    }
                }
                if (mKEY instanceof GifFragment){
                    Response<Gif> response = ApiConfig.getClient().getGits().execute();
                    if (response.body() != null){
                        return response.body();
                    }
                }

                if (mKEY instanceof AboutFragment){
                    Response<About> response = ApiConfig.getClient().getAbout().execute();
                    if (response.body() != null){
                        return response.body();
                    }
                }
            }catch (Exception e){
                return e;
            }
            return null;
        }



        @Override
        protected void onPostExecute(Object object) {
            super.onPostExecute(object);
            if (object != null && object instanceof Latest || object instanceof Category || object instanceof Gif || object instanceof About){
                mListener.onResquest(object);
            } else if(object != null && object instanceof  Exception){
                mListener.onError(((Exception) object).getMessage());
            }
        }
    }



    public static class RequestDataDetail extends AsyncTask<Void , Void, Object>{
        private DataListener mListener;
        private Fragment mKEY;
        private String mId;

        public RequestDataDetail(DataListener listener, Fragment key, String id) {
            mListener = listener;
            mKEY = key;
            mId = id;
        }



        @Override
        protected Object doInBackground(Void... voids) {
            try{
                if (mKEY instanceof DetailCategoryFragment){
                    Response<DetailCategory> response = ApiConfig.getClient().getCategoryList(mId).execute();
                    if (response.body() != null){
                        return response.body();
                    }
                }
                if (mKEY instanceof DetailGifFragment){
                    Response<Gif> response = ApiConfig.getClient().getGifList(mId).execute();
                    if (response.body() != null){
                        return response.body();
                    }
                }

            }catch (Exception e){
                return e;
            }
            return null;
        }



        @Override
        protected void onPostExecute(Object object) {
            super.onPostExecute(object);
            if (object != null && object instanceof DetailCategory || object instanceof Gif){
                mListener.onResquest(object);
            } else if(object != null && object instanceof  Exception){
                mListener.onError(((Exception) object).getMessage());
            }
        }
    }
}
