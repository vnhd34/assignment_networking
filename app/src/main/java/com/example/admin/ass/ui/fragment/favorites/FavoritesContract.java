package com.example.admin.ass.ui.fragment.favorites;

import android.content.Context;

import com.example.admin.ass.event.ClickListener;
import com.example.admin.ass.model.favorites.MyFavorites;
import com.example.admin.ass.model.latest.HdWallPaper;
import com.example.admin.ass.mvp.basefragment.BaseMvpContract;

import java.util.List;

public interface FavoritesContract {

    interface View extends BaseMvpContract.View<Presenter>{

        void setUpRecyclerView(List<MyFavorites> hdwallpaper);

        void showDialogLoading();

        void hideDialogLoading();

        void goToDetailActivity(MyFavorites hdWallPaper);

        Context getContextActivity();
    }
    
    interface Presenter extends ClickListener {
        void setUpData();
    }
}
