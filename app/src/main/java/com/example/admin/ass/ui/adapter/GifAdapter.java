package com.example.admin.ass.ui.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.admin.ass.R;
import com.example.admin.ass.event.ClickListener;
import com.example.admin.ass.model.gif.HDWALLPAPER;
import com.example.admin.ass.util.ImageHelper;
import com.facebook.drawee.view.SimpleDraweeView;

import java.io.UnsupportedEncodingException;
import java.util.List;

public class GifAdapter extends RecyclerView.Adapter<GifAdapter.ViewHolder> {
    private List<HDWALLPAPER> mWallPapers;
    private Activity mContext;
    private ClickListener mListener;



    public GifAdapter(List<HDWALLPAPER> wallPapers, Activity context) {
        mWallPapers = wallPapers;
        mContext = context;
    }



    public void onClickItem(ClickListener listener){
        mListener = listener;
    }



    @NonNull
    @Override
    public GifAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.layout_item_image, parent, false));
    }



    @Override
    public void onBindViewHolder(@NonNull GifAdapter.ViewHolder holder, int position) {
        try {
            holder.bindWitthData(mWallPapers.get(position));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }



    @Override
    public int getItemCount() {
        return mWallPapers.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private SimpleDraweeView mImage;
        private TextView mSee;
        private TextView mLike;
        int[] mScreen;

        public ViewHolder(View itemView) {
            super(itemView);
            mImage = itemView.findViewById(R.id.imageSimpleDrawee);
            mSee = itemView.findViewById(R.id.seenTextview);
            mLike = itemView.findViewById(R.id.likedTextview);
            mScreen = ImageHelper.getDeviceMetrics(mContext);
            itemView.setOnClickListener(this);
        }

        public void bindWitthData(HDWALLPAPER hdWallPaper) throws UnsupportedEncodingException {
            ImageHelper.loadImage(hdWallPaper.getGifImage().replaceAll(" ", "%20"), mImage, mScreen[0]/3, mScreen[0]/3);
            mImage.getHierarchy().setProgressBarImage(ImageHelper.progressBarLoading(mContext));
            mSee.setText(hdWallPaper.getTotalViews());
            mLike.setText(hdWallPaper.getId());
        }



        @Override
        public void onClick(View view) {
            mListener.onClickItem(getAdapterPosition());
        }
    }


}
