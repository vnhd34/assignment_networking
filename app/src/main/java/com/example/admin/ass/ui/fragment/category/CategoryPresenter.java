package com.example.admin.ass.ui.fragment.category;


import com.example.admin.ass.event.DataListener;
import com.example.admin.ass.model.category.Category;
import com.example.admin.ass.model.category.HDWALLPAPER;
import com.example.admin.ass.mvp.basefragment.BaseMvpPresenter;
import com.example.admin.ass.rest.ApiRequest;

import java.util.List;

public class CategoryPresenter extends BaseMvpPresenter<CategoryContract.View> implements CategoryContract.Presenter {

    private List<HDWALLPAPER> mList;

    public CategoryPresenter(CategoryContract.View view) {
        super(view);
    }



    @Override
    public void setUpData() {
        new ApiRequest.RequestData(listener, new CategoryFragment()).execute();
        mView.showDialogLoading();
    }

    private DataListener listener = new DataListener() {
        @Override
        public void onResquest(Object target) {
            if (target instanceof Category){
                mList = ((Category) target).getHDWALLPAPER();
                mView.setUpRecyclerView(mList);
                mView.hideDialogLoading();
            }
        }



        @Override
        public void onError(String error) {
            mView.showToast(error);
            mView.hideDialogLoading();
        }
    };



    @Override
    public void onClickItem(int position) {
        mView.goToFragmemntDetailCategory(mList.get(position).getCid());
    }
}
