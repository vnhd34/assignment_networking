
package com.example.admin.ass.model.latest;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HdWallPaper implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("cat_id")
    @Expose
    private String catId;
    @SerializedName("wallpaper_image")
    @Expose
    private String wallpaperImage;
    @SerializedName("wallpaper_image_thumb")
    @Expose
    private String wallpaperImageThumb;
    @SerializedName("total_views")
    @Expose
    private String totalViews;
    @SerializedName("cid")
    @Expose
    private String cid;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("category_image")
    @Expose
    private String categoryImage;
    @SerializedName("category_image_thumb")
    @Expose
    private String categoryImageThumb;



    public HdWallPaper(String id, String catId, String wallpaperImage, String wallpaperImageThumb, String totalViews, String cid, String categoryName, String categoryImage, String categoryImageThumb) {
        this.id = id;
        this.catId = catId;
        this.wallpaperImage = wallpaperImage;
        this.wallpaperImageThumb = wallpaperImageThumb;
        this.totalViews = totalViews;
        this.cid = cid;
        this.categoryName = categoryName;
        this.categoryImage = categoryImage;
        this.categoryImageThumb = categoryImageThumb;
    }



    protected HdWallPaper(Parcel in) {
        id = in.readString();
        catId = in.readString();
        wallpaperImage = in.readString();
        wallpaperImageThumb = in.readString();
        totalViews = in.readString();
        cid = in.readString();
        categoryName = in.readString();
        categoryImage = in.readString();
        categoryImageThumb = in.readString();
    }



    public static final Creator<HdWallPaper> CREATOR = new Creator<HdWallPaper>() {
        @Override
        public HdWallPaper createFromParcel(Parcel in) {
            return new HdWallPaper(in);
        }



        @Override
        public HdWallPaper[] newArray(int size) {
            return new HdWallPaper[size];
        }
    };



    public String getId() {
        return id;
    }



    public void setId(String id) {
        this.id = id;
    }



    public String getCatId() {
        return catId;
    }



    public void setCatId(String catId) {
        this.catId = catId;
    }



    public String getWallpaperImage() {
        return wallpaperImage;
    }



    public void setWallpaperImage(String wallpaperImage) {
        this.wallpaperImage = wallpaperImage;
    }



    public String getWallpaperImageThumb() {
        return wallpaperImageThumb;
    }



    public void setWallpaperImageThumb(String wallpaperImageThumb) {
        this.wallpaperImageThumb = wallpaperImageThumb;
    }



    public String getTotalViews() {
        return totalViews;
    }



    public void setTotalViews(String totalViews) {
        this.totalViews = totalViews;
    }



    public String getCid() {
        return cid;
    }



    public void setCid(String cid) {
        this.cid = cid;
    }



    public String getCategoryName() {
        return categoryName;
    }



    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }



    public String getCategoryImage() {
        return categoryImage;
    }



    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }



    public String getCategoryImageThumb() {
        return categoryImageThumb;
    }



    public void setCategoryImageThumb(String categoryImageThumb) {
        this.categoryImageThumb = categoryImageThumb;
    }



    @Override
    public int describeContents() {
        return 0;
    }



    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(catId);
        parcel.writeString(wallpaperImage);
        parcel.writeString(wallpaperImageThumb);
        parcel.writeString(totalViews);
        parcel.writeString(cid);
        parcel.writeString(categoryName);
        parcel.writeString(categoryImage);
        parcel.writeString(categoryImageThumb);
    }
}
