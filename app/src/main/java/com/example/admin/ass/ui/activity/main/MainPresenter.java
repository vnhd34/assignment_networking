package com.example.admin.ass.ui.activity.main;

import com.example.admin.ass.mvp.baseactivity.BaseMvpPresenter;

public class MainPresenter extends BaseMvpPresenter<MainContract.View> implements MainContract.Presenter {

    public MainPresenter(MainContract.View view) {
        super(view);
    }
}
