package com.example.admin.ass.ui.fragment.favorites;

import com.example.admin.ass.db.SQLiteHelperMe;
import com.example.admin.ass.model.favorites.MyFavorites;
import com.example.admin.ass.model.latest.HdWallPaper;
import com.example.admin.ass.model.latest.Latest;
import com.example.admin.ass.mvp.basefragment.BaseMvpPresenter;
import com.example.admin.ass.util.FakeData;

import java.util.List;

public class FavoritesPresenter extends BaseMvpPresenter<FavoritesContract.View> implements FavoritesContract.Presenter {
    private List<MyFavorites> mList;
    private SQLiteHelperMe mDB;

    public FavoritesPresenter(FavoritesContract.View view) {
        super(view);

    }



    @Override
    public void setUpData() {
        mDB = SQLiteHelperMe.getInstance(mView.getContextActivity());
        mList = mDB.getAllFavorites();
        mView.setUpRecyclerView(mList);
    }



    @Override
    public void onClickItem(int position) {
        mView.goToDetailActivity(mList.get(position));
    }
}
