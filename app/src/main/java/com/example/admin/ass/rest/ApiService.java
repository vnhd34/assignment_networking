package com.example.admin.ass.rest;

import com.example.admin.ass.model.about.About;
import com.example.admin.ass.model.category.Category;
import com.example.admin.ass.model.detailcategory.DetailCategory;
import com.example.admin.ass.model.gif.Gif;
import com.example.admin.ass.model.latest.Latest;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    @GET("api.php?latest")
    Call<Latest> getLatest();

    @GET("api.php?cat_list")
    Call<Category> getCategory();

    @GET("api.php")
    Call<DetailCategory> getCategoryList(@Query("cat_id") String id);

    @GET("api.php?gif_list")
    Call<Gif> getGits();

    @GET("api.php")
    Call<Gif> getGifList(@Query("gif_list") String id);

    @GET("api.php")
    Call<About> getAbout();

}
