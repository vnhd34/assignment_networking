package com.example.admin.ass.ui.fragment.gif;

import com.example.admin.ass.event.DataListener;
import com.example.admin.ass.model.gif.Gif;
import com.example.admin.ass.model.gif.HDWALLPAPER;
import com.example.admin.ass.model.latest.Latest;
import com.example.admin.ass.mvp.basefragment.BaseMvpPresenter;
import com.example.admin.ass.rest.ApiRequest;

import java.util.List;


public class GifPresenter extends BaseMvpPresenter<GifContract.View> implements GifContract.Presenter {

    private List<HDWALLPAPER> mList;

    public GifPresenter(GifContract.View view) {
        super(view);
    }



    @Override
    public void setUpData() {
        new ApiRequest.RequestData(listener, new GifFragment()).execute();
        mView.showDialogLoading();
    }



    private DataListener listener = new DataListener() {
        @Override
        public void onResquest(Object target) {
            if (target instanceof Gif) {
                mList = ((Gif) target).getHDWALLPAPER();
                mView.hideDialogLoading();
                mView.setUpRecyclerView(mList);
            }
        }



        @Override
        public void onError(String error) {
            mView.hideDialogLoading();
            mView.showToast(error);
        }
    };



    @Override
    public void onClickItem(int position) {
        mView.goToFragmemntDetailGif(mList.get(position).getId());
    }
}
