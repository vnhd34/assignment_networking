package com.example.admin.ass.ui.fragment.about;

import com.example.admin.ass.event.DataListener;
import com.example.admin.ass.model.about.About;
import com.example.admin.ass.model.about.HDWALLPAPER;
import com.example.admin.ass.mvp.basefragment.BaseMvpPresenter;
import com.example.admin.ass.rest.ApiRequest;

public class AboutPresenter extends BaseMvpPresenter<AboutContract.View> implements AboutContract.Presenter {
    public AboutPresenter(AboutContract.View view) {
        super(view);
    }


    @Override
    public void setUpData() {
        new ApiRequest.RequestData(listener, new AboutFragment()).execute();
    }


    private DataListener listener = new DataListener() {
        @Override
        public void onResquest(Object target) {
            About hdwallpaper = (About) target;
            mView.setUpViewWithData(hdwallpaper.getHDWALLPAPER().get(0));
        }



        @Override
        public void onError(String error) {

        }
    };
}
