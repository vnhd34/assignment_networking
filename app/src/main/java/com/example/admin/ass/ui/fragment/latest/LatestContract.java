package com.example.admin.ass.ui.fragment.latest;

import com.example.admin.ass.event.ClickListener;
import com.example.admin.ass.model.latest.HdWallPaper;
import com.example.admin.ass.mvp.basefragment.BaseMvpContract;

import java.util.List;

public interface LatestContract {

    interface View extends BaseMvpContract.View<Presenter>{

        void setUpRecyclerView(List<HdWallPaper> hdwallpaper);

        void showDialogLoading();

        void hideDialogLoading();

        void goToDetailActivity(HdWallPaper hdWallPaper);
    }
    
    interface Presenter extends ClickListener {

        void setUpData();
    }
}
