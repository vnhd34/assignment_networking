package com.example.admin.ass.ui.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.admin.ass.R;
import com.example.admin.ass.event.ClickListener;
import com.example.admin.ass.model.latest.HdWallPaper;
import com.example.admin.ass.util.ImageHelper;
import com.facebook.drawee.view.SimpleDraweeView;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

public class LatestAdapter extends RecyclerView.Adapter<LatestAdapter.ViewHolder> {
    private List<HdWallPaper> mWallPapers;
    private Activity mContext;
    private ClickListener mListener;



    public LatestAdapter(List<HdWallPaper> wallPapers, Activity context) {
        mWallPapers = wallPapers;
        mContext = context;
    }



    public void onClickListener(ClickListener clickListener) {
        mListener = clickListener;
    }



    @NonNull
    @Override
    public LatestAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.layout_item_image, parent, false));
    }



    @Override
    public void onBindViewHolder(@NonNull LatestAdapter.ViewHolder holder, int position) {
        holder.bindWitthData(mWallPapers.get(position));
    }



    @Override
    public int getItemCount() {
        return mWallPapers.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private SimpleDraweeView mImage;
        private TextView mSee;
        private TextView mLike;
        int[] mScreen;



        public ViewHolder(View itemView) {
            super(itemView);
            mImage = itemView.findViewById(R.id.imageSimpleDrawee);
            mSee = itemView.findViewById(R.id.seenTextview);
            mLike = itemView.findViewById(R.id.likedTextview);
            mScreen = ImageHelper.getDeviceMetrics(mContext);
            itemView.setOnClickListener(this);
        }



        public void bindWitthData(HdWallPaper hdWallPaper) {
            ImageHelper.loadImage(hdWallPaper.getWallpaperImage().replaceAll(" ", "%20"), mImage, mScreen[0] / 3, mScreen[0] / 3);
            mImage.getHierarchy().setProgressBarImage(ImageHelper.progressBarLoading(mContext));
            mSee.setText(hdWallPaper.getTotalViews());
            mLike.setText(hdWallPaper.getCid());
            if ((getAdapterPosition()+1) >= 2 && (getAdapterPosition()+1) % 5 == 0){
                mLike.setTextColor(Color.RED);
                mSee.setTextColor(Color.RED);
            }
        }



        @Override
        public void onClick(View view) {
            mListener.onClickItem(getAdapterPosition());
        }
    }


}
