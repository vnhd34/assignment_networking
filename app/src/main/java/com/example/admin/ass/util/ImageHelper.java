package com.example.admin.ass.util;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;

import com.example.admin.ass.R;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.drawable.ProgressBarDrawable;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.image.ImageInfo;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import java.lang.reflect.Field;

public class ImageHelper {

    public static void loadImage(String url, SimpleDraweeView simpleDraweeView, int width, int height) {

        Uri avatarURI = Uri.parse(url);

        ImageRequest request = ImageRequestBuilder.newBuilderWithSource(avatarURI)
                .setResizeOptions(new ResizeOptions(width, height))
                .setLocalThumbnailPreviewsEnabled(true)
                .setLowestPermittedRequestLevel(ImageRequest.RequestLevel.FULL_FETCH)
                .setProgressiveRenderingEnabled(false)
                .build();

        simpleDraweeView.setController(
                Fresco.newDraweeControllerBuilder()
                        .setOldController(simpleDraweeView.getController())
                        .setImageRequest(request)
                        .setAutoPlayAnimations(true)
                        .build());
    }



    public static int[] getDeviceMetrics(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        return new int[]{width, height};
    }



    public static ProgressBarDrawable progressBarLoading(Context context) {
        ProgressBarDrawable progressBarDrawable = new ProgressBarDrawable();
        progressBarDrawable.setColor(context.getResources().getColor(R.color.colorAccent));
        progressBarDrawable.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        progressBarDrawable.setRadius(5);
        return progressBarDrawable;
    }
}
