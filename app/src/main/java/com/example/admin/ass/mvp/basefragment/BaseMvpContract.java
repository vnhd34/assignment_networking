package com.example.admin.ass.mvp.basefragment;

import android.support.v4.app.Fragment;

public interface BaseMvpContract {
    interface View<P> {
        void initView(android.view.View view);

        void afterInit();

        P getPreseter();

        void showToast(String messager);

        void switchActivity(Class<?> target);

        void switchFragment(int parentId, Fragment fragment);
    }

    interface Presenter {

    }
}
