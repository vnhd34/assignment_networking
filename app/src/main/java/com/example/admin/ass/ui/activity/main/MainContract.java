package com.example.admin.ass.ui.activity.main;

import com.example.admin.ass.mvp.baseactivity.BaseMvpContract;

public interface MainContract {
    interface View extends BaseMvpContract.View<Presenter>{

    }

    interface Presenter{

    }
}
