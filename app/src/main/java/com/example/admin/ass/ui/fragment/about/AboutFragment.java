package com.example.admin.ass.ui.fragment.about;

import android.view.View;
import android.widget.TextView;

import com.example.admin.ass.R;
import com.example.admin.ass.model.about.HDWALLPAPER;
import com.example.admin.ass.mvp.basefragment.BaseMvpFragment;

public class AboutFragment extends BaseMvpFragment<AboutContract.Presenter> implements  AboutContract.View {

    private TextView mAboutVer;
    private TextView mAboutConpany;
    private TextView mAboutEmail;
    private TextView mAboutWeb;
    private TextView mAboutContact;
    private TextView mAboutContent;
    public AboutFragment() {
        super(R.layout.fragment_about);
    }



    @Override
    public void initView(View view) {

        mAboutVer = view.findViewById(R.id.tv_about_ver);
        mAboutConpany = view.findViewById(R.id.tv_about_conpany);
        mAboutEmail = view.findViewById(R.id.tv_about_email);
        mAboutWeb = view.findViewById(R.id.tv_about_web);
        mAboutContact = view.findViewById(R.id.tv_about_contact);
        mAboutContent = view.findViewById(R.id.tv_about_content);

    }



    @Override
    public void afterInit() {
        mPresenter.setUpData();
    }



    @Override
    public AboutContract.Presenter getPreseter() {
        return new AboutPresenter(this);
    }



    @Override
    public void setUpViewWithData(HDWALLPAPER data) {
        mAboutVer.setText(data.getAppVersion());
        mAboutEmail.setText(data.getAppEmail());
        mAboutWeb.setText(data.getAppWebsite());
        mAboutContact.setText(data.getAppContact());
        String texxt = data.getAppDescription();
        mAboutContent.setText(data.getAppDescription());
    }
}
