package com.example.admin.ass.ui.fragment.gif;

import android.app.ProgressDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.admin.ass.R;
import com.example.admin.ass.model.gif.HDWALLPAPER;
import com.example.admin.ass.model.latest.HdWallPaper;
import com.example.admin.ass.mvp.basefragment.BaseMvpFragment;
import com.example.admin.ass.ui.adapter.GifAdapter;
import com.example.admin.ass.ui.adapter.LatestAdapter;
import com.example.admin.ass.ui.fragment.detailcategory.DetailCategoryFragment;
import com.example.admin.ass.ui.fragment.detailgif.DetailGifFragment;

import java.util.List;

public class GifFragment extends BaseMvpFragment<GifContract.Presenter> implements  GifContract.View {

    private RecyclerView mRecyclerView;
    private GifAdapter mAdapter;
    private ProgressDialog mDialog;

    public GifFragment() {
        super(R.layout.fragment_gif);
    }



    @Override
    public void initView(View view) {
        mRecyclerView = view.findViewById(R.id.rv);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
    }



    @Override
    public void afterInit() {
        mPresenter.setUpData();
    }



    @Override
    public GifContract.Presenter getPreseter() {
        return new GifPresenter(this);
    }



    @Override
    public void setUpRecyclerView(List<HDWALLPAPER> hdwallpaper) {
        mAdapter = new GifAdapter(hdwallpaper, getActivity());
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.onClickItem(mPresenter);
    }



    @Override
    public void showDialogLoading() {
        mDialog = new ProgressDialog(getActivity());
        mDialog.setTitle("Loading");
        mDialog.setMessage("Hold on");
        mDialog.show();
    }



    @Override
    public void hideDialogLoading() {
        mDialog.dismiss();
    }



    @Override
    public void goToFragmemntDetailGif(String id) {
        DetailGifFragment fragment = new DetailGifFragment();
        fragment.getImage(id);
        switchFragment(R.id.myLayout, fragment);
    }
}
