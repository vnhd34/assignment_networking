package com.example.admin.ass.ui.fragment.detailgif;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.admin.ass.R;
import com.example.admin.ass.model.gif.HDWALLPAPER;
import com.example.admin.ass.model.latest.HdWallPaper;
import com.example.admin.ass.mvp.basefragment.BaseMvpFragment;
import com.example.admin.ass.ui.activity.imagedetail.ImagDetailActivity;
import com.example.admin.ass.ui.activity.main.MainActivity;
import com.example.admin.ass.ui.adapter.GifAdapter;
import com.example.admin.ass.ui.adapter.LatestAdapter;

import java.util.List;

public class DetailGifFragment extends BaseMvpFragment<DetailGifContract.Presenter> implements  DetailGifContract.View {

    private RecyclerView mRecyclerView;
    private GifAdapter mAdapter;
    private ProgressDialog mDialog;

    public DetailGifFragment() {
        super(R.layout.fragment_detail_category);
    }



    @Override
    public void initView(View view) {
        mRecyclerView = view.findViewById(R.id.rv);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
    }



    @Override
    public void afterInit() {
        MainActivity.mToolbar.setTitle("Detail Gif");
    }



    @Override
    public DetailGifContract.Presenter getPreseter() {
        return new DetailGifPresenter(this);
    }



    @Override
    public void setUpRecyclerView(List<HDWALLPAPER> hdwallpaper) {
        mAdapter = new GifAdapter(hdwallpaper, getActivity());
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.onClickItem(mPresenter);
    }



    @Override
    public void showDialogLoading() {
        mDialog = new ProgressDialog(getActivity());
        mDialog.setTitle("Loading");
        mDialog.setMessage("Hold on");
        mDialog.show();
    }



    @Override
    public void hideDialogLoading() {
        mDialog.dismiss();
    }



    @Override
    public void goToDetailActivity(HDWALLPAPER hdWallPaper) {
        Intent intent = new Intent(getActivity(), ImagDetailActivity.class);
        intent.putExtra(ImagDetailActivity.KEY_IMAGE, hdWallPaper );
        startActivity(intent);
    }


    public void getImage(String id){
        mPresenter.setUpData(id);
    }
}
