package com.example.admin.ass.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import com.example.admin.ass.model.favorites.MyFavorites;

import java.util.ArrayList;
import java.util.List;

public class SQLiteHelperMe extends SQLiteOpenHelper {

    private static SQLiteHelperMe sInstance;
    // Database Info
    private static final String DATABASE_NAME = "wallpaper";
    private static final int DATABASE_VERSION = 1;

    // Table Names
    private static final String TABLE = "favorites";

    // Post Table Columns
    private static final String KEY_ID = "idimage";
    private static final String KEY_URL = "urlimage";
    private static final String KEY_VIEW = "viewmimage";
    private static final String KEY_LIKE = "likeimage";


    @Override
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);
        db.setForeignKeyConstraintsEnabled(true);
    }



    public static synchronized SQLiteHelperMe getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new SQLiteHelperMe(context.getApplicationContext());
        }
        return sInstance;
    }


    private SQLiteHelperMe(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }



    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        String CREATE_TABLE = " CREATE TABLE " + TABLE +
                "(" +
                KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
                KEY_URL + " NVARCHAR(100)," +
                KEY_VIEW + " NVARCHAR(100)," +
                KEY_LIKE + " NVARCHAR(100)"+
                ")";

        sqLiteDatabase.execSQL(CREATE_TABLE);
    }



    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion != newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE);
            onCreate(db);
        }
    }



    public void addPost(MyFavorites favorites, Context context) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_URL, favorites.getUrl());
            values.put(KEY_VIEW, favorites.getView());
            values.put(KEY_LIKE, favorites.getLike());
            db.insertOrThrow(TABLE, null, values);
            db.setTransactionSuccessful();
            Toast.makeText(context, "Đã lưu vào Favorites" , Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Toast.makeText(context, "Có lỗi xảy ra" , Toast.LENGTH_LONG).show();
            Log.d("SQL", "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }
    }



    public List<MyFavorites> getAllFavorites() {
        List<MyFavorites> list = new ArrayList<>();
        String POSTS_SELECT_QUERY = "SELECT * FROM " + TABLE;

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(POSTS_SELECT_QUERY, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    MyFavorites favorites = new MyFavorites();
                    favorites.setUrl(cursor.getString(cursor.getColumnIndex(KEY_URL)));
                    favorites.setLike(cursor.getString(cursor.getColumnIndex(KEY_LIKE)));
                    favorites.setView(cursor.getString(cursor.getColumnIndex(KEY_VIEW)));

                    list.add(favorites);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.d("SQL", "Error while trying to get posts from database");
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return list;
    }


    public void delete(String id) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            db.delete(TABLE, KEY_URL + " = ?", new String[] { String.valueOf(id)});
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.d("", "Error while trying to delete all posts and users");
        } finally {
            db.endTransaction();
        }
    }
}
